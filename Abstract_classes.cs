﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            Console.WriteLine(dog.Describe());
            Console.ReadKey();
        }
    }

    abstract class FourLeggedAnimal
    {
        public virtual string Describe()
        {
            return "It's an animal with four legs";
        }
    }

    class Dog : FourLeggedAnimal
    {
        public override string Describe()
        {
            string result = base.Describe();
            result += " It is, in fact, a dog!";
            return result;
        }
    }
}