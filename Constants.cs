﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constants
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The worst advice from a friend: " + SomeClass.TheWorstAdviceFromAFriend);
            Console.WriteLine("The best advice from a friend: " + SomeClass.GetAnswer());
        }
    }

    class SomeClass
    {
        private const string TheBestAdviceFromAFriend = "Don't smoke anything! It will cause only bad things in your body!";
        public const string TheWorstAdviceFromAFriend = "Go ahead and smoke a cigarette. Nothing bad will happen!";

        public static string GetAnswer()
        {
            return TheBestAdviceFromAFriend;
        }
    }
}