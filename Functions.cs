﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {
        public void DoStuff()
        {
            Console.WriteLine("Can anyone read this?");
        }
    }
}