﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {
        public int AddNumbers(int number1, int number2)
        {
            int result = number1 + number2;
            if (result > 10)
            {
                return result;
            }
            return 0;
        }
    }
}