﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> listOfUsers = new List<User>()
        {
        new User() { Name = "Jake Owen", Age = 42 },
        new User() { Name = "Natalie Edwards", Age = 34 },
        new User() { Name = "Sarah York", Age = 8 },
        };

            for (int i = 0; i < listOfUsers.Count; i++)
            {
                Console.WriteLine(listOfUsers[i].Name + " is " + listOfUsers[i].Age + " years old");
            }
            Console.ReadKey();
        }
    }

    class User
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}