﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 5;

            do
            {
                Console.WriteLine(number);
                number = number + 1;
            } while (number < 15);
        }
    }
}