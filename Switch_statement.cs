﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you think we should vote for PSD? (yes/no/maybe)");
            string input = Console.ReadLine();
            switch (input.ToLower())
            {
                case "yes":
                case "maybe":
                    Console.WriteLine("Get lost!");
                    break;
                case "no":
                    Console.WriteLine("Agree!");
                    break;
                default:
                    Console.WriteLine("I'm sorry, I don't understand that!");
                    break;
            }
        }
    }
}